# WTest

## Accomplisments:

A aplicação está feita em kotlin e usa uma arquitectura simples View -> View Model -> Model (MVVM)

### Exercise 1
A aplicação abre um Splash screen e depois passa para a Main screen.
Aqui inicia o download do ficheiro [condigos_postais.csv](https://github.com/centraldedados/codigos_postais/blob/master/data/codigos_postais.csv)
Room, para guardar os dados na base de dados interna, está parcialmente integrado, mas não está funcional.
Para o download do ficheiro estou a user Retrofit.

## Resultado final:
- A Aplicação tem um edit text e um icon que seria usado para a pesquisa dos códigos.
- Os dados são downloaded, mas não é feito o parse, logo não há dados a serem mostrados.

Isabel