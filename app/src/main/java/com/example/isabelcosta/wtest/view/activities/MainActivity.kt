package com.example.isabelcosta.wtest.view.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.widget.Toast
import com.example.isabelcosta.wtest.R
import com.example.isabelcosta.wtest.viewmodels.PostalCodesViewModel

class MainActivity : BaseActivity() {

    private lateinit var viewModel: PostalCodesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(PostalCodesViewModel::class.java)

        viewModel.successful.observe(this, Observer {
                successful ->
            this.hideProgressDialog()
            if (successful != null) {
                if (successful) {
                    Toast.makeText(this@MainActivity, "Postal_Codes.csv download was a success!", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this@MainActivity, "Postal_Codes.csv download was a failure!", Toast.LENGTH_SHORT).show()
                }
            }
        })


        showProgressDialog(getString(R.string.downloading_codes_file))
        viewModel.downloadPostalCodesFile()

    }
}
