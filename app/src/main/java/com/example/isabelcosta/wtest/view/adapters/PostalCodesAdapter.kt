package com.example.isabelcosta.wtest.view.adapters

import android.support.annotation.NonNull
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.isabelcosta.wtest.R
import com.example.isabelcosta.wtest.data.models.PostalCode
import kotlinx.android.synthetic.main.item_postal_code.view.*

/**
 * This class represents the adapter that fills in each view of the Postal Codes recyclerView
 * @param postalCodesList list of postal codes to show
 */
class PostalCodesAdapter (
    private val postalCodesList: List<PostalCode>
) : RecyclerView.Adapter<PostalCodesAdapter.PostalCodesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostalCodesViewHolder =
        PostalCodesViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_postal_code, parent, false)
        )

    override fun onBindViewHolder(@NonNull holder: PostalCodesViewHolder, position: Int) {
        val item = postalCodesList[position]
        val itemView = holder.itemView

        itemView.tvPostalCode.text = item.postalCode
    }

    override fun getItemCount(): Int = postalCodesList.size

    /**
     * This class holds a view for each item of the Postal Codes list
     * @param itemView represents each view of Postal Codes list
     */
    class PostalCodesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
