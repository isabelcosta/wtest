package com.example.isabelcosta.wtest.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [PostalCodeDao::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun postalCodeDao(): PostalCodeDao

    companion object {
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (instance == null) {
                synchronized(AppDatabase::class) {
                    instance = Room.databaseBuilder(context,
                        AppDatabase::class.java, "PostalCodes.db")
                        .build()
                }
            }
            return instance
        }

        fun destroyInstance() {
            instance = null
        }
    }
}