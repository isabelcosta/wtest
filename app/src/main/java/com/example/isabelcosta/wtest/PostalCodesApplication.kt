package com.example.isabelcosta.wtest

import android.app.Application
import android.content.Context
import android.support.v7.app.AppCompatDelegate

class PostalCodesApplication : Application() {

    companion object {

        lateinit var instance: PostalCodesApplication

        /**
         * @return the instance of the Application
         */
        fun getApplication(): PostalCodesApplication {
            return instance
        }

        /**
         * @return the context of the Application
         */
        fun getContext(): Context {
            return instance.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        instance = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
    }
}