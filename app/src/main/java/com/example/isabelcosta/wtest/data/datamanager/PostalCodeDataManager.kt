package com.example.isabelcosta.wtest.data.datamanager

import com.example.isabelcosta.wtest.PostalCodesApplication
import com.example.isabelcosta.wtest.data.database.AppDatabase
import com.example.isabelcosta.wtest.data.models.PostalCode
import com.example.isabelcosta.wtest.data.remote.ApiManager
import okhttp3.ResponseBody
import retrofit2.Call

/**
 * This class represents the data manager related to Postal Code CSV download and saving into Database
 */
class PostalCodeDataManager {

    private val apiManager: ApiManager = ApiManager()
//    private val localManager: AppDatabase = AppDatabase.getInstance(PostalCodesApplication.getContext())!!

    /**
     * This will use PostalCodeService to fetch all postal codes
     * @return an Observable of a list of [PostalCode]
     */
    fun getPostalCodes(url: String): Call<ResponseBody> {
        return apiManager.downloadCsvService.getPostalCodeCsv(url)
    }

    /**
     * This will parse and save postal codes into Database
     * @return an Observable of a list of [PostalCode]
     */
    fun parseAndSavePostalCodes(url: String): Call<ResponseBody> {
        return apiManager.downloadCsvService.getPostalCodeCsv(url)
    }
}