package com.example.isabelcosta.wtest.data.remote

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

/**
 * This interface describes the methods used to download files
 */
interface DownloadCsvService {
    /**
     * This function returns a CSV file which contains Portuguese postal codes
     * @return an observable instance of [PostalCode]s CSV file
     */
    @Streaming
    @GET
    fun getPostalCodeCsv(@Url fileUrl: String): Call<ResponseBody>
}