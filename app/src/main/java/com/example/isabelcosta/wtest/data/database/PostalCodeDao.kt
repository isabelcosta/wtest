package com.example.isabelcosta.wtest.data.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.isabelcosta.wtest.data.models.PostalCode
import java.nio.charset.CodingErrorAction.REPLACE

@Dao
interface PostalCodeDao {
    @Query("SELECT * FROM postal_code")
    fun getAll(): List<PostalCode>

    //    @Insert(onConflict = REPLACE)
    @Insert
    fun insertAll(postalCodes: List<PostalCode>)

//        @Insert(onConflict = REPLACE)
    @Insert
    fun insert(postalCode: PostalCode)

    @Query("DELETE FROM postal_code")
    fun deleteAll()

    @Delete
    fun delete(postalCode: PostalCode)
}