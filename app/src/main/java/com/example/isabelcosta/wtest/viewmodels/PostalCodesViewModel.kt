package com.example.isabelcosta.wtest.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.example.isabelcosta.wtest.data.datamanager.PostalCodeDataManager
import com.example.isabelcosta.wtest.data.models.PostalCode
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.os.AsyncTask
import com.example.isabelcosta.wtest.PostalCodesApplication
import java.io.*


/**
 * This class represents the [ViewModel] component used for the MainActivity
 */
class PostalCodesViewModel : ViewModel() {

    companion object {
        const val POSTAL_CODE_CSV_URL_PATH = "centraldedados/codigos_postais/master/data/codigos_postais.csv"
        const val POSTAL_CODE_CSV_FILENAME = "postalcodes.csv"
    }

    var TAG = PostalCodesViewModel::class.java.simpleName

    private val postalCodeDataManager: PostalCodeDataManager = PostalCodeDataManager()
    private val context = PostalCodesApplication.getContext()

    val successful: MutableLiveData<Boolean> = MutableLiveData()
    lateinit var message: String
    lateinit var postalCodesList: List<PostalCode>

    fun downloadPostalCodesFile() {
        val call = postalCodeDataManager.getPostalCodes(POSTAL_CODE_CSV_URL_PATH)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    Log.d(TAG, "server contacted and has file")

                    val downloadFileTask =
                    object : AsyncTask<Void, Void, Boolean>() {
                        override fun doInBackground(vararg voids: Void): Boolean? {
                            val writtenToDisk =
                                writeResponseBodyToDisk(response.body()!!)

                            Log.d(TAG, "File download was a success? $writtenToDisk")
                            return writtenToDisk
                        }

                        override fun onPostExecute(result: Boolean?) {
                            super.onPostExecute(result)

                            // todo update LiveData obj to announce download is complete

                        }
                    }
                    downloadFileTask.execute()
                } else {
                    Log.d(TAG, "Server contact failed")
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d(TAG, "Server contact failed")
            }
        })
    }

    private fun parsePostalCodes(br: BufferedReader) {

        // todo read file
        // todo parse

        var line = br.readLine()
        val comma = ","

        while (line != null) {

            val postalCodeFullLine = line.split(comma)
            val lastIndex = postalCodeFullLine.size - 1

            val firstCode = postalCodeFullLine[lastIndex - 2]
            val secondCode = postalCodeFullLine[lastIndex - 1]
            val localName = postalCodeFullLine[lastIndex]
            // todo: sanitize these values
            val postalCode = PostalCode(postalCode = "$firstCode-$secondCode, $localName")

            // todo: save postal code into database

            line = br.readLine()
        }
    }

    private fun writeResponseBodyToDisk(body: ResponseBody): Boolean {
        try {
            val futureStudioIconFile = File(context.filesDir.path + File.separator + POSTAL_CODE_CSV_FILENAME)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                val fileSize = body.contentLength()
                var fileSizeDownloaded: Long = 0

                inputStream = body.byteStream()
                outputStream = FileOutputStream(futureStudioIconFile)

                while (true) {
                    val read = inputStream!!.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)

                    fileSizeDownloaded += read.toLong()

                    Log.d(TAG, "File download: $fileSizeDownloaded of $fileSize")
                }

                outputStream.flush()

                return true
            } catch (e: IOException) {
                return false
            } finally {
                inputStream?.close()
                outputStream?.close()
            }
        } catch (e: IOException) {
            return false
        }

    }
}